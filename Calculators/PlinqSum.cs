﻿using System.Linq;

namespace MultiThreading
{
    internal class PlinqSum : ICalculate
    {
        private readonly int[] _array;

        public PlinqSum(int[] array)
        {
            _array = array;
        }

        public int Calculate()
        {
            return _array.AsParallel().Sum();
        }
    }
}
