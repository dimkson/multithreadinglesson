﻿namespace MultiThreading
{
    internal class SingleThreadSum : ICalculate
    {
        private readonly int[] _array;

        public SingleThreadSum(int[] array)
        {
            _array = array;
        }

        public int Calculate()
        {
            int sum = 0;
            for (int i = 0; i < _array.Length; i++)
            {
                sum += _array[i];
            }
            return sum;
        }
    }
}
