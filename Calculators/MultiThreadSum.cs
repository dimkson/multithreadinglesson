﻿using System.Collections.Generic;
using System.Threading;

namespace MultiThreading
{
    internal class MultiThreadSum : ICalculate
    {
        private readonly int[] _array;
        private readonly int _threadCount;
        private readonly object _lock;

        public int TotalSum { get; private set; }

        public MultiThreadSum(int[] array, int threadCount)
        {
            _array = array;
            _threadCount = threadCount;
            _lock = new();
        }

        public int Calculate()
        {
            List<Thread> threads = new();

            for (int i = 0; i < _threadCount; i++)
            {
                threads.Add(new Thread(GetSum));
                threads[i].Start(i);
            }

            threads.ForEach(t => t.Join());

            return TotalSum;
        }

        private void GetSum(object obj)
        {
            int position = (int)obj;
            int step = _array.Length / _threadCount;
            int startPosition = step * position;
            int endPosition = step * (position + 1);
            int sum = 0;

            for (int i = startPosition; i < endPosition; i++)
            {
                sum += _array[i];
            }

            // Учет элементов, оставшихся в случае невозможности целочисленного деления массива на кол-во потоков
            if (position == _threadCount - 1)
            {
                int ost = _array.Length % step;
                if (ost != 0)
                {
                    for (int i = _array.Length - ost; i < _array.Length; i++)
                    {
                        sum += _array[i];
                    }
                }
            }

            lock (_lock)
            {
                TotalSum += sum;
            }
        }
    }
}
