﻿using System.Diagnostics;

namespace MultiThreading
{
    internal class SpeedTest
    {
        private int[] _array;
        private int _count;
        private Stopwatch _watch;
        private ILogger _logger;

        public SpeedTest(int count, ILogger logger)
        {
            _array = DataGenerator.GenerateArray(count);
            _count = count;
            _watch = new();
            _logger = logger;
        }

        public void StartTest()
        {
            long time = Test(new SingleThreadSum(_array));
            _logger.Log($"OneThread.\tВремя выполнения: {time}.\tРазмер: {_count}");
            time = Test(new MultiThreadSum(_array, 4));
            _logger.Log($"MultiThread.\tВремя выполнения: {time}.\tРазмер: {_count}");
            time = Test(new PlinqSum(_array));
            _logger.Log($"PLINQ.\t\tВремя выполнения: {time}.\tРазмер: {_count}");
        }

        private long Test(ICalculate calculator)
        {
            _watch.Restart();
            int totalSum = calculator.Calculate();
            _watch.Stop();
            //_logger.Log($"Сумма: {totalSum}");
            return _watch.ElapsedMilliseconds;
        }
    }
}
