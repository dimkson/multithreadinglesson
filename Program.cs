﻿namespace MultiThreading
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            ConsoleLogger logger = new();
            SpeedTest speedTest = new(100_000, logger);
            speedTest.StartTest();

            speedTest = new(1_000_000, logger);
            speedTest.StartTest();

            speedTest = new(10_000_000, logger);
            speedTest.StartTest();

            speedTest = new(100_000_000, logger);
            speedTest.StartTest();
        }
    }
}