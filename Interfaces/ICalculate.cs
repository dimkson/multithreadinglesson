﻿namespace MultiThreading
{
    internal interface ICalculate
    {
        int Calculate();
    }
}
