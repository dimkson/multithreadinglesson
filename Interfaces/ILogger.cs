﻿namespace MultiThreading
{
    internal interface ILogger
    {
        void Log(string text);
    }
}
