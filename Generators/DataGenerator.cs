﻿using System;

namespace MultiThreading
{
    internal static class DataGenerator
    {
        public static int[] GenerateArray(int length)
        {
            var array = new int[length];

            var random = new Random();
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = random.Next(0, 10);
            }

            return array;
        }
    }
}
